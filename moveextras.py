#!/usr/bin/python3
import argparse
import os
import pathlib

SUBDIR_NAME="extras"

def debug(debugInfo):
	if(args.debug):
		print ("INFO: ", debugInfo)

def ensureSubdir():
	if not os.path.exists(SUBDIR_NAME):
		os.mkdir(SUBDIR_NAME)

parser = argparse.ArgumentParser(description='Converts the MakeMKV output to be aligned with the Jellyfin naming conventions. The biggest file in the target directory will be renamed to the directory name and the others will be moved \
	to a new subfolder named "'+SUBDIR_NAME+'".')
parser.add_argument('target_directories', default='.', nargs='*',
                    help='list of one or more directories containing the MakeMKV output (default: current directory)')
parser.add_argument('--debug', action='store_true', help='echo debug information')
parser.add_argument('--dry-run', '-n', action='store_true', dest='skip_actions', help='perform a trial run with no changes made')

args = parser.parse_args()
debug(args)

for target in args.target_directories:
	if not os.path.isdir(target):
		print("ERROR: target_directory \"{0}\"is not a directory. Aborting.".format(target))
		exit(1)


originalWorkingDirectory = os.getcwd()
debug("originalWorkingDirectory: "+originalWorkingDirectory)

for targetDirectory in args.target_directories:
	os.chdir(originalWorkingDirectory)
	os.chdir(targetDirectory)
	cwd=os.getcwd()
	debug("activeDirectory: "+cwd)


	# Storing list of all files
	# in the given directory in files
	files = filter( lambda x: os.path.isfile
	                       (os.path.join(cwd, x)),
	                        os.listdir() )
	# Sort list of file names by size 
	files = sorted( files,
	                        key =  lambda x: os.stat
	                       (os.path.join(cwd, x)).st_size, reverse=True)
	debug(files)
	if len(files) == 0: 
		print("No files found in directory \"{0}\".".format(cwd))
		continue

	largest_file_name=files.pop(0)
	largest_file_extension = pathlib.Path(largest_file_name).suffix
	debug("File extension: "+largest_file_extension)
	largest_file_new_name=os.path.basename(cwd)+largest_file_extension
	debug("New File Name: "+largest_file_new_name)

	number_of_renamed_files=0
	if not os.path.exists(largest_file_new_name):
		if not (args.skip_actions):
			os.rename(largest_file_name, largest_file_new_name)
		number_of_renamed_files+=1


	number_of_moved_files=0
	for filename in files:
		if not (args.skip_actions):
			ensureSubdir()
			os.rename(filename, SUBDIR_NAME+"/"+filename)
		number_of_moved_files+=1

	print("Renamed {0} file(s) and moved {1} file(s) in \"{2}\".".format(number_of_renamed_files, number_of_moved_files,targetDirectory))

exit(0)