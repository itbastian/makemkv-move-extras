# makemkv-move-extras

MakeMKV extracts several mkv files. The biggest one will be renamed to the directory name and the others will be moved to a new subfolder named extras. 
That is compliant with Jellyfins naming convention.